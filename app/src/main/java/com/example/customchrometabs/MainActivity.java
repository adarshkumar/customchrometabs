package com.example.customchrometabs;

import androidx.appcompat.app.AppCompatActivity;
import androidx.browser.customtabs.CustomTabColorSchemeParams;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.content.ContextCompat;

import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import static androidx.browser.customtabs.CustomTabsIntent.*;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void launchCCT(View view) {
        EditText editText = findViewById(R.id.url);
        String url = editText.getText().toString();
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        CustomTabColorSchemeParams params = new CustomTabColorSchemeParams.Builder()
                .setNavigationBarColor(ContextCompat.getColor(this, R.color.white))
                .setToolbarColor(ContextCompat.getColor(this, R.color.purple_700))
                .setSecondaryToolbarColor(ContextCompat.getColor(this, R.color.purple_500))
                .build();

        builder.setColorSchemeParams(CustomTabsIntent.COLOR_SCHEME_LIGHT, params);
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(this, Uri.parse(url));
    }
}